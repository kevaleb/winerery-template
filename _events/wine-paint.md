---
name: Wine & Paint Night
date: 2018-03-07 18:00:00
event_image: /images/events/wine-paint.jpg
---
Join us for a night of wine & paint with friends. We'll be serving an
  assortment of cheese and crackers to pair with your wine and unveiling our
  newest bottling for the first time.


  Ticket price is $40/Person. Can't wait to see you there.