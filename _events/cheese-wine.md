---
name: Cheese & Wine
date: 2018-03-28 20:00:00
event_image: /images/events/wine-cheese.jpg
---

Join us for an afternoon of wine and cheese. Discover the joys of finding just  the right cheese to pair with your favorite wines and learn what makes them go  so well together.

$15.00 per person (club members receive 20% discount)