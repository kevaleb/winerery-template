---
title: Test-Wine-3
winename: Cabernet Franc
description: This is a red cab with 80% this and 20% that
region: Forgotten Coast
wine-color: Red
image_path: /images/wine/bottle-7.jpg
video_path: 'https://www.youtube.com/embed/7lf5QDTdNUE'
featured: True
price: $36

---