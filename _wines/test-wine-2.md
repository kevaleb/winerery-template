---
title: Test-Wine-2
winename: Petite Syrah
description: This is a white chardonnay with 80% this and 20% that
region: Forgotten Coast
wine-color: White
image_path: /images/wine/bottle-2.jpg
video_path: 'https://www.youtube.com/embed/7lf5QDTdNUE'
featured: True
price: $24
---