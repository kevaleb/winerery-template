---
title: Test-Wine-3
winename: Sangiovese
description: This is a red cab with 80% this and 20% that
region: Forgotten Coast
wine-color: Red
image_path: /images/wine/bottle-3.jpg
video_path: 
featured: true
price: $18
---